
# josemorenoasix/generate-tls

This image generates a **self-signed** SSL/TLS certificate:
  * cacert.pem
  * server.key
  * server.pem

  > **TIP**: Edit **openssl.cnf** and change default values.

## Examples  

Generating  certificate for server "**app.mydomain.tld**" using default values:

```
docker run --rm -v $(pwd):/certificates -e "SERVER=app.mydomain.tld" josemorenoasix/generate-tls
```

Another option, you can set CA Organization values:

```
docker run --rm -v $(pwd):/certificates -e "SERVER=app.mydomain.tld" -e "SUBJECT=/C=CA/ST=Canada/L=Canada/O=IT" josemorenoasix/generate-tls
```
